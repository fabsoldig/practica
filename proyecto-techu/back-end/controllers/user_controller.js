var requestJSON = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/techu6db/collections/";
const apikeyMLab = "apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";

module.exports.getUsers = getUsers;

//GET Consulta de usuarios usando el API REST de Mlab
//app.get(URL_BASE + 'users', function(req, res) {
function getUsers(req, res){
   console.log("GET /apitechu/v2/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log("Hay --> "+body.length +" usuarios en la tabla");
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});
