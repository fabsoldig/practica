/* Archivo server.js (backend)
   Autor: Diego CLemente - dclemente@bbva.com
   Curso: Practitioner Tech-U
*/
require('dotenv').config();

var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');

var app = express();
app.use(bodyParser.json());
var totalUsers = 0;
var cuerpo;
const URL_BASE = '/apitechu/v2/';
const PORT = process.env.PORT || 3000;

const baseMLabURL = "https://api.mlab.com/api/1/databases/techu6db/collections/";
const apikeyMLab = "apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";
//const apikeyMLab = "apiKey="+process.env.MLAB_API_KEY;

const user_controller = require('./controllers/user_controller');

//GET Consulta de usuarios usando el API REST de Mlab
//app.get(URL_BASE + 'users',user_controller.getUsers);
app.get(URL_BASE + 'users',
 function(req, res) {
   console.log("GET /apitechu/v2/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log("Hay --> "+body.length +" usuarios en la tabla");
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// Petición GET con id en mLab
 app.get(URL_BASE + 'users/:id',
   function (req, res) {
     console.log("GET /apitechu/v2/users/:id");
    console.log(req.params.id);
     var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         console.log("Respuesta mLab correcta.");
         var respuesta = body[0];
         res.send(respuesta);
      });
 });


 // POST 'users' mLab
app.post(URL_BASE + 'users',
 function(req, res){
   console.log("POST /apitechu/v2/users");
   var clienteMlab = requestJSON.createClient(baseMLabURL);
   console.log(req.body);
   clienteMlab.get('user?' + apikeyMLab,
     function(error, respuestaMLab, body){
       newID = body.length + 1;
       console.log("newID:" + newID);
       var newUser = {
         "id" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
       };
       console.log("nuevo: "+JSON.stringify(newUser));

       clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
         function(error, respuestaMLab, body){
           console.log("Respuesta mLab correcta para el registro del usuario");
           console.log(body);
           res.status(201);
           res.send(body);
         });
     });
 });

 /*
     * Y este texto puede decir lo que yo quiera
     * para acordarme de algo
     */

 //PUT users con parámetro 'id'
 //'set' solo sobreescribe los campos enviados por el body
 app.put(URL_BASE + 'users/:id',
 function(req, res) {
  var id = req.params.id;
  console.log("Se actualizara el usuario con id -> "+id);
  var queryStringID = 'q={"id":' + id + '}&'; //Se consulta el usuario por id enviado por querystring
  var clienteMlab = requestJSON.createClient(baseMLabURL);
  clienteMlab.get('user?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log("cambio: "+cambio);

     clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
       //res.status(200).send(body);
       res.send(body);
     });
    });
 });

 // Petición PUT con id de mLab (_id.$oid)
  app.put(URL_BASE + 'usersmLab/:id',
    function (req, res) {
      var id = req.params.id;
      console.log("Se actualizara el usuario con id -> "+id);
      let userBody = req.body;
      var queryString = 'q={"id":' + id + '}&';
      //var queryString = 'q={"first_name":"Pepe"}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body){
          let response = body[0];
          console.log(body);
          //Actualizo campos del usuario
          let updatedUser = {
            "id" : req.body.id,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : req.body.password
          };//Otra forma simplificada (para muchas propiedades)
          // var updatedUser = {};
          // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
          // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
          // PUT a mLab
          httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
            function(err, respuestaMLab, body){
              var response = {};
              if(err) {
                  response = {
                    "msg" : "Error actualizando usuario."
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg" : "Usuario actualizado correctamente."
                  }
                  res.status(200);
                }
              }
              res.send(response);
            });
        });
  });


  //DELETE user with id
  app.delete(URL_BASE + "users/:id",
   function(req, res){
     var id = req.params.id;
     console.log("Se eliminara el usuario con id -> "+id);
     var queryStringID = 'q={"id":' + id + '}&';
     console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' +  queryStringID + apikeyMLab,
       function(error, respuestaMLab, body){
         var respuesta = body[0];
         console.log("usuario recuperado: "+JSON.stringify(respuesta));
         httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
           function(error, respuestaMLab,body){
             console.log("SE elimino con exito");
             res.send(body);
         });
       });
   });


//Method POST login
   app.post(URL_BASE + "login",
    function (req, res){
      console.log("POST /apitechu/v2/login");
      let email = req.body.email;
      let pass = req.body.password;
      let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
      let limFilter = 'l=1&';
      let clienteMlab = requestJSON.createClient(baseMLabURL);
      clienteMlab.get('user?'+ queryString + limFilter + apikeyMLab,
        function(error, respuestaMLab, body) {
          if(!error) {
            if (body.length == 1) { // Existe un usuario que cumple 'queryString'
              let login = '{"$set":{"logged":true}}';
              console.log(JSON.stringify(body[0]));
              clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(login),
              //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
                function(errPut, resPut, bodyPut) {
                  res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
                  // If bodyPut.n == 1, put de mLab correcto
                });
            }
            else {
              res.status(404).send({"msg":"Usuario no válido."});
            }
          } else {
            res.status(500).send({"msg": "Error en petición a mLab."});
          }
      });
   });


   //Method POST LOGOUT
      app.post(URL_BASE + "/logout/:id",
       function (req, res){
         console.log("POST /apitechu/v2/logout");

        var id = req.params.id;
        console.log("Desloguearemos al usuario con id -> "+id);
        var queryStringID = 'q={"id":' + id + '}&';
        console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);

        var httpClient = requestJSON.createClient(baseMLabURL);
        httpClient.get('user?' +  queryStringID + apikeyMLab,
          function(error, respuestaMLab, body){
            //var respuesta = body[0];
            console.log("usuario recuperado: "+JSON.stringify(respuesta));

            if(!error) {
              if (body.length == 1) { // Existe un usuario que cumple 'queryString'
                let login = '{"$unset":{"logged":true}}';
                console.log(JSON.stringify(body[0]));
                clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(login),
                //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
                  function(errPut, resPut, bodyPut) {
                    res.send({'msg':'Logout correcto', 'user':body[0].email, 'userid':body[0].id});
                    // If bodyPut.n == 1, put de mLab correcto
                  });
              }
              else {
                res.status(404).send({"msg":"Usuario no válido."});
              }
            } else {
              res.status(500).send({"msg": "Error en petición a mLab."});
            }


          });

      });



//Peticion get de todos los usuarios (Collections)
/*app.get(URL_BASE + '/users',
    function(request, response){
      console.log('GET '+URL_BASE + ' users');
      response.send(userFile);
 });*/

//Peticion GET de un usuario especifico (INstancia)
/*app.get(URL_BASE + '/users/:id',
    function(request, response){
      console.log('GET '+URL_BASE + ' users/id');
      console.log(request.params.id);
      let indice = request.params.id;
      let instancia = userFile[indice-1];
      console.log(instancia);
      let respuesta = (instancia!= undefined)? instancia: {"mensaje" :"Recurso no encontrado"};
      response.status(200); //codigo de error http
      response.send (respuesta);
});*/

//Peticion GET Ccon query string
/*app.get(URL_BASE + 'usersq',
    function(request, response){
      console.log('GET '+URL_BASE + ' con query string');
      console.log(request.query);
      response.send (response);
});*/

//Peticion POST para crear un usuario
/*app.post(URL_BASE + '/users',
  function(req, res){
    console.log('POST '+URL_BASE + ' users');
  totalUsers = userFile.length;
  cuerpo = req.body;
    let newUser = {
      userID : totalUsers,
      first_name : req.body.first_name,
      last_name : req.body.last_name,
      email : req.body.email,
      password: req.body.password
    }
    userFile.push(newUser);
    res.status(201);
    res.send({"mensaje":"Usuario creado con exito", "usuario": newUser});

});*/

//Peticion PUT para actualizar datos de un usuario
/*app.put(URL_BASE + '/users/:id',
  function(req, res){
  console.log('PUT '+URL_BASE + 'users/:id');

  let indice = req.params.id-1;
  if(userFile[indice] != undefined){
    userFile[indice].first_name = req.body.first_name;
    userFile[indice].last_name = req.body.last_name;

    res.status(201);
    res.send({"mensaje":"Se ha actualizado con exitoo", "userACtualizado": userFile[indice] });
  }else{
    res.send({"mensaje":"NO se ha encontrado el usuario requerido para su actualizacion"});
  }
});*/


//Peticion PUT para actualizar datos de un usuario
/*app.delete(URL_BASE + '/users/:id',
  function(req, res){
  console.log('DELETE '+URL_BASE + 'users/:id');
  totalUsers = userFile.length;
  console.log("Hay: "+totalUsers + " usuarios");
  let indice = req.params.id-1;
  if(userFile[indice] != undefined){
    console.log("Usuario a eliminar: "+userFile[indice].userID+" - " +userFile[indice].first_name);

    userFile.splice(indice,1);

    console.log("Ahora hay: "+userFile.length + " usuarios");
    for (i = 0; i < userFile.length; i++) {
      console.log("id:"+userFile[i].userID + "  firstName: " + userFile[i].first_name);
    }

    res.status(201);
    res.send({"mensaje":"Se ha eliminado con exitoo" });
  }else{
    res.send({"mensaje":"NO se ha encontrado el usuario a eliminar"});
  }
});*/

//Login usuario
//Peticion POST para LOGIN
/*app.post(URL_BASE + '/login',
  function(req, res){
    console.log('POST '+URL_BASE + ' login');
    let user = req.body.email;
    let clave = req.body.password;
    console.log("user: " +user);
    console.log("clave: " +clave);

    for (usuario of userFile) {
        if(usuario.email == user){
          if(usuario.password == clave){
		usuario.logueado = true;
              	//console.log("usuario: "+JSON.stringify(usuario));

                res.status(201);
    		res.send({"mensaje":"LOgin correcto", "usuario": JSON.stringify(usuario)});

		//Escribimos en el archivo
   		writeUserDataToFile(userFile);

		break;

          }else{
           res.send({"mensaje":"Login incorrecto"});

          }
        }
    }
});*/

//LOgout usuario
//Peticion POST para logout
app.put(URL_BASE + '/logout/:id',
  function(req, res){
    console.log('POST '+URL_BASE + ' logout');
    	let idUsuario = req.params.id-1;
 	console.log("idUsario:"+idUsuario);

	for (usuario of userFile) {
		if(usuario.userID == idUsuario){
		console.log("--antes a eliminar: "+JSON.stringify(usuario));
		usuario.logged = false;
		console.log("--despues a eliminar: "+JSON.stringify(usuario));
	 	res.status(201);
		res.send({"mensaje":"Logout correcto", "usuario": JSON.stringify(usuario)});
		break;
		}else{
			res.send({"mensaje":"NO se pudo desloguear"});
		}
	}

});

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   //console.log("json: "+jsonUserData);

   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'user.json'.");
      }
    })
 }

//Server debdddddde escuchar datos a consumir
app.listen(PORT, function(){
    console.log('APItechu iniciando .... ');
});
